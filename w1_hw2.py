def sum(x, y): #định nghĩa tổng của 2 biến x,y
    sum = x + y
    if sum in range(15, 20): # nếu tổng trong khoảng (15,20)
        return 20 #trả kết quả về 20
    else:
        return sum # nếu tổng ngoài (15,20) thì trả về kết quả là x+y

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))
#output lần lượt là
#20 vì 10+16 =26 >20 return 20
#12 vì 10+2 = 12 < 15 return sum
#22 vì 10+12= 22> 20  return sum