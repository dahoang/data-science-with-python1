import datetime #nhập vào datetime từ thư viện python
now = datetime.datetime.now()
print ("Current date and time : ")
# in ra thời gian hiện tại theo chuỗi thời gian
#%Y-%m -%d là định nghĩ năm - tháng - ngày
#%H:%M:%s là định nghĩa giờ: phút: giây
print (now.strftime("%Y-%m-%d %H:%M:%S"))
#output
#Current date and time :
#2018-10-01 20:00:32
